![](https://gitlab.com/sjrowlinson/reslate/raw/master/images/reslate_logo.png)

An automatic API documentation pages generator for Python projects with Sphinx-based docs.

- Follow the latest changes: https://gitlab.com/sjrowlinson/reslate
