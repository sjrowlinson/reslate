{header_break}
``{name}``
{header_break}

.. automodule:: {module_name}

.. currentmodule:: {module_name}

{classes}

{dataclasses}

{enums}

{functions}
