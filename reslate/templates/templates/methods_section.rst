.. rubric:: Methods

.. autosummary::
    :toctree: generated/

    {constructor}
    {methods}
